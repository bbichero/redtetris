import React from 'react';
import { connect } from 'react-redux';
import { Header } from '../components/header/header';
import { Home } from '../components/home/home';
import { Party } from '../components/party/party';
import { Link, Route, BrowserRouter} from 'react-router-dom';
import '../components/@styles/app.scss';

const App = () => {
  return (
    <BrowserRouter>
      <div className="app">
        <Header/>

        <div className="navigation-button">
          <Link to="/"> <button className="button"> Home </button> </Link>

          <Link to="/party"> <button className="button"> Party </button> </Link>
        </div>

        <Route exact path="/" component={Home}/>
        <Route exact path="/party" component={Party}/>

      </div>
    </BrowserRouter>
  );
};

const mapStateToProps = (state) => {
  return {
    message: state.message
  };
};

export default connect(mapStateToProps, null)(App);
