// React dependencies
import React from 'react';

// Style dependencies
import './party.scss';

// Component dependencies
import {
  Board
} from '../board/board';

/**
 * Component that create the party zone
 */
export const Party = () => {
  // Return the render of the component
  return (
    <div className="party-container">
      <Board/>
    </div>
  );
};
