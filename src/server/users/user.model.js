import mongoose from "mongoose";

/*
 *	User schema
 */
 const userSchema = new mongoose.Schema({
	login: {
		type: String,
		required: true,
	},
	cookie: {
		type: String,
		required: true
	},
	socketId: {
		type: String,
		required: true
	},
	createAt: {
		type: Date,
		default: Date.now
	},
	updatedAt: {
		type: Date,
		default: null
	}
});

/*
 *	User schema, rewrite basic functions
 */
userSchema.statics = {
	get(id) {
		return this.findById(id)
			.exec()
			.then((user) => {
				if (user)
					return user;
				return false;
			});
	},
	list() {
		return this.find({})
			.exec();
	}
};

export default mongoose.model("User", userSchema);
