// Import node module
import express from "express";
import * as UserCtrl from "./user.controller";
const router = express.Router();

/**
 * @api {get} /users/		All users list
 * @apiName GetUsers
 * @apiDescription		List all users created
 * @apiGroup Users
 *
 * @apiSuccess {Object} lobbies	Created Users
 */
router.get("/", UserCtrl.list);

/**
 * @api {post} /users/add	Add new user
 * @apiName AddUser
 * @apiDescription		Add a new user in DB
 * @apiGroup Users
 *
 * @apiParam {String} login	Login of user
 * @apiParam {String} socketId	Socket Id from user
 *
 * @apiSuccess {Boolean} status Boolean status of the document creation
 * @apiSuccess {String}	userId	Id of new user created
 */
router.post("/add", UserCtrl.create);

/**
 * @api {get} /users/:userId	User information
 * @apiName GetUser
 * @apiDescription		Get informations of a particular user
 * @apiGroup Users
 *
 * @apiParam {Number}	userId	MongoDB Id of a particular user
 *
 * @apiSuccess {Object} User	Created user
 */
router.route("/:userId")
	.get(UserCtrl.get)

/**
 * @api {put} /users/:userId	Edit User information
 * @apiName EditUser
 * @apiDescription		Edit informations of a particular user
 * @apiGroup Users
 *
 * @apiParam {String} login	User login
 * @apiParam {String} cookie	User's cookie
 * @apiParam {String} socketId	Socket Id from user
 *
 * @apiSuccess {Object} User	New user information
 */
	.put(UserCtrl.update);

// When userId is sent in param, call load function and append user to request 
router.param("userId", UserCtrl.load);

// Exporting an object as the default import for this module
export default router;
