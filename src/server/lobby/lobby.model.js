import mongoose from "mongoose";

/*
 *	Lobby schema
 */
 const lobbySchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
	},
	usersId: {
		type: Array,
		required: true
	},
	status: {
		type: String,
		enum: ["created", "waiting", "start", "finish", "archived"],
		default: "created"
	},
	createAt: {
		type: Date,
		default: Date.now
	},
	updatedAt: {
		type: Date,
		default: null
	}
});

/*
 *	List all lobby created
 */
lobbySchema.statics = {
	get(id) {
		return this.findById(id)
			.exec()
			.then((lobby) => {
				if (lobby)
					return lobby;
				return false;
			});
	},
	list() {
		return this.find({})
			.exec();
	}
};

export default mongoose.model("Lobby", lobbySchema);
